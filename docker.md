// Docker 允许你在容器内运行应用程序， 使用 docker run 命令来在容器内运行一个应用程序。

/**输出Hello world

docker run ubuntu:15.10 /bin/echo "Hello world"

各个参数解析：

docker: Docker 的二进制执行文件。

run: 与前面的 docker 组合来运行一个容器。

ubuntu:15.10 指定要运行的镜像，Docker 首先从本地主机上查找镜像是否存在，如果不存在，Docker 就会从镜像仓库 Docker Hub 下载公共镜像。

/bin/echo "Hello world": 在启动的容器里执行的命令

以上命令完整的意思可以解释为：Docker 以 ubuntu15.10 镜像创建一个新容器，然后在容器里执行 bin/echo "Hello world"，然后输出结果。

我们通过 docker 的两个参数 -i -t，让 docker 运行的容器实现"对话"的能力：
docker run -i -t ubuntu:15.10 /bin/bash
各个参数解析：

-t: 在新容器内指定一个伪终端或终端。

-i: 允许你对容器内的标准输入 (STDIN) 进行交互。

注意第二行 root@0123ce188bd8:/#，此时我们已进入一个 ubuntu15.10 系统的容器

我们尝试在容器中运行命令 cat /proc/version和ls分别查看当前系统的版本信息和当前目录下的文件列表
我们可以通过运行 exit 命令或者使用 CTRL+D 来退出容器。

root@0123ce188bd8:/#  exit
exit
root@runoob:~# 
注意第三行中 root@runoob:~# 表明我们已经退出了当前的容器，返回到当前的主机中。
使用以下命令创建一个以进程方式运行的容器
docker run -d ubuntu:15.10 /bin/sh -c "while true; do echo hello world; sleep 1; done"
在输出中，我们没有看到期望的 "hello world"，而是一串长字符

2b1b7a428627c51ab8810d541d759f072b4fc75487eed05812646b8534a2fe63

这个长字符串叫做容器 ID，对每个容器来说都是唯一的，我们可以通过容器 ID 来查看对应的容器发生了什么。

首先，我们需要确认容器有在运行，可以通过 docker ps 来查看：
runoob@runoob:~$ docker ps
CONTAINER ID        IMAGE                  COMMAND              ...  
5917eac21c36        ubuntu:15.10           "/bin/sh -c 'while t…"    ...
输出详情介绍：

CONTAINER ID: 容器 ID。

IMAGE: 使用的镜像。

COMMAND: 启动容器时运行的命令。

CREATED: 容器的创建时间。

STATUS: 容器状态。

状态有7种：

created（已创建）
restarting（重启中）
running 或 Up（运行中）
removing（迁移中）
paused（暂停）
exited（停止）
dead（死亡）
PORTS: 容器的端口信息和使用的连接类型（tcp\udp）。

NAMES: 自动分配的容器名称。

在宿主主机内使用 docker logs 命令，查看容器内的标准输出：
我们使用 docker stop 命令来停止容器:

docker 客户端非常简单 ,我们可以直接输入 docker 命令来查看到 Docker 客户端的所有命令选项。

可以通过命令 docker command --help 更深入的了解指定的 Docker 命令使用方法。

例如我们要查看 docker stats 指令的具体使用方法：

如果我们本地没有 ubuntu 镜像，我们可以使用 docker pull 命令来载入 ubuntu 镜像：

以下命令使用 ubuntu 镜像启动一个容器，参数为以命令行模式进入该容器：

docker run -it ubuntu /bin/bash

参数说明：

-i: 交互式操作。
-t: 终端。
ubuntu: ubuntu 镜像。
/bin/bash：放在镜像名后的是命令，这里我们希望有个交互式 Shell，因此用的是 /bin/bash。
要退出终端，直接输入 exit:

查看所有的容器命令如下：

docker ps -a

使用 docker start 启动一个已停止的容器：

在大部分的场景下，我们希望 docker 的服务是在后台运行的，我们可以过 -d 指定容器的运行模式。

docker run -itd --name ubuntu-test ubuntu /bin/bash

注：加了 -d 参数默认不会进入容器，想要进入容器需要使用指令 docker exec（下面会介绍到）。

停止容器的命令如下：
docker stop <容器 ID>

停止的容器可以通过 docker restart 重启：
docker restart <容器 ID>

在使用 -d 参数时，容器启动后会进入后台。此时想要进入容器，可以通过以下指令进入：

docker attach

docker exec：推荐大家使用 docker exec 命令，因为此命令会退出容器终端，但不会导致容器的停止。

attach 命令

下面演示了使用 docker attach 命令。

注意： 如果从这个容器退出，会导致容器的停止。

下面演示了使用 docker exec 命令。

注意： 如果从这个容器退出，容器不会停止，这就是为什么推荐大家使用 docker exec 的原因。

更多参数说明请使用 docker exec --help 命令查看。

如果要导出本地某个容器，可以使用 docker export 命令。

docker export 1e560fca3906 > ubuntu.tar

可以使用 docker import 从容器快照文件中再导入为镜像，以下实例将快照文件 ubuntu.tar 导入到镜像 test/ubuntu:v1:

cat docker/ubuntu.tar | docker import - test/ubuntu:v1

此外，也可以通过指定 URL 或者某个目录来导入，例如：

docker import http://example.com/exampleimage.tgz example/imagerepo

删除容器使用 docker rm 命令：

docker rm -f 1e560fca3906

运行一个 web 应用

前面我们运行的容器并没有一些什么特别的用处。

接下来让我们尝试使用 docker 构建一个 web 应用程序。

我们将在docker容器中运行一个 Python Flask 应用来运行一个web应用。

docker pull training/webapp  # 载入镜像

docker run -d -P training/webapp python app.py

参数说明:

-d:让容器在后台运行。

-P:将容器内部使用的网络端口随机映射到我们使用的主机上。

使用 docker ps 来查看我们正在运行的容器：

runoob@runoob:~#  docker ps
CONTAINER ID        IMAGE               COMMAND             ...        PORTS                 
d3d5e39ed9d3        training/webapp     "python app.py"     ...        0.0.0.0:32769->5000/tcp

这里多了端口信息。

PORTS
0.0.0.0:32769->5000/tcp
Docker 开放了 5000 端口（默认 Python Flask 端口）映射到主机端口 32769 上。

这时我们可以通过浏览器访问WEB应用

我们也可以通过 -p 参数来设置不一样的端口：

runoob@runoob:~$ docker run -d -p 5000:5000 training/webapp python app.py

通过 docker ps 命令可以查看到容器的端口映射，docker 还提供了另一个快捷方式 docker port，使用 docker port 可以查看指定 （ID 或者名字）容器的某个确定端口映射到宿主机的端口号。

上面我们创建的 web 应用容器 ID 为 bf08b7f2cd89 名字为 wizardly_chandrasekhar。

我可以使用 docker port bf08b7f2cd89 或 docker port wizardly_chandrasekhar 来查看容器端口的映射情况。

查看 WEB 应用程序日志

docker logs [ID或者名字] 可以查看容器内部的标准输出。

-f: 让 docker logs 像使用 tail -f 一样来输出容器内部的标准输出。

从上面，我们可以看到应用程序使用的是 5000 端口并且能够查看到应用程序的访问日志。

我们还可以使用 docker top 来查看容器内部运行的进程

使用 docker inspect 来查看 Docker 的底层信息。它会返回一个 JSON 文件记录着 Docker 容器的配置和状态信息。

停止 WEB 应用容器
runoob@runoob:~$ docker stop wizardly_chandrasekhar 

重启WEB应用容器
已经停止的容器，我们可以使用命令 docker start 来启动。

docker ps -l 查询最后一次创建的容器：

正在运行的容器，我们可以使用 docker restart 命令来重启。

移除WEB应用容器
我们可以使用 docker rm 命令来删除不需要的容器

删除容器时，容器必须是停止状态，否则会报如下错误

runoob@runoob:~$ docker rm wizardly_chandrasekhar
Error response from daemon: You cannot remove a running container bf08b7f2cd897b5964943134aa6d373e355c286db9b9885b1f60b6e8f82b2b85. Stop the container before attempting removal or force remove


**/
